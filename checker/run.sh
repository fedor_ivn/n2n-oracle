export IMAGE=registry.gitlab.com/onti-fintech/onti-2021-fintech/fintech2021004/n2n-oracle/ftchecker-final2021-n2n-oracle
export TAG=latest
export SOLUTION_PATH=../../n2n-oracle

docker-compose -p ftchecker up -d
docker logs -f ftchecker_n2n-oracle_1
docker-compose stop
cp -r ./logs ../$CI_PROJECT_NAME/logs
exit $(docker wait ftchecker_n2n-oracle_1)