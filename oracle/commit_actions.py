import asyncio
from typing import NamedTuple, Dict, Set, Optional, Tuple
from logging import getLogger, INFO, basicConfig
from enum import Enum
from eth_utils import remove_0x_prefix
from dotenv import load_dotenv
from multiprocessing import Queue
from colorama import Style, Fore
from web3 import Web3

from settings import OracleConfig
from utils.client import Client
from utils.contract import DeployedContract

logger = getLogger("oracle.worker")

ACTION_ALREADY_PERFORMED = "The action was already perfomed."

class WorkerConfig(NamedTuple):
    actions: Dict[bytes, Tuple[str, int]]
    my_address: str
    first_transaction_nonce: int


class WorkerResult:
    def __init__(self):
        self.failed_actions = set()
        self.detected_nonce_race = False


class Result(Enum):
    OK = 0
    FAILED = 1
    DETECTED_NONCE_RACE = 2


def run(worker_config: WorkerConfig, queue: Queue):
    """
    This is only meant to be the target function when using multiprocessing.
    """

    load_dotenv()
    basicConfig(
        format=(
            f"{Style.DIM}%(asctime)s "
            f"{Style.RESET_ALL}{Style.BRIGHT}%(levelname)s "
            f"{Style.RESET_ALL}{Fore.GREEN}%(name)s{Style.DIM}/"
            f"{Style.RESET_ALL}{Style.BRIGHT}{Fore.GREEN}%(funcName)s"
            f"{Style.RESET_ALL}: %(message)s"
        )
    )
    logger.setLevel(INFO)
    oracle_config = OracleConfig()
    client = Client(
        oracle_config.destination_rpc_url,
        oracle_config.private_key,
        oracle_config.gas_price,
    )
    contract = DeployedContract(
        client,
        address=oracle_config.destination_address,
        abi=oracle_config.build["abi"]
    )

    result = asyncio.run(
        commit_actions(worker_config, client, contract),
    )
    queue.put(result)


async def commit_aсtions(
    worker_config: WorkerConfig,
    contract: DeployedContract,
    transaction_lock: Optional[asyncio.Lock] = None,
):
    transaction_nonce = worker_config.first_transaction_nonce - 1

    def next_transaction_nonce():
        nonlocal transaction_nonce
        transaction_nonce += 1
        return transaction_nonce

    worker_result = WorkerResult()

    async def collector(id: bytes, recipient: str, amount: int):
        try:
            result = await commit_action(
                worker_config,
                contract,
                recipient, amount, Web3().toHex(id),
                next_transaction_nonce,
                transaction_lock,
            )
        except Exception as error:
            logger.error(f"failed to commit {id}: {error}")
            worker_result.failed_actions.add(id)
            return

        if result != Result.OK:
            worker_result.failed_actions.add(id)
        if result == Result.DETECTED_NONCE_RACE:
            worker_result.detected_nonce_race = True

    coroutines = (
        collector(id, recipient, amount)
        for id, (recipient, amount)
        in worker_config.actions.items()
    )
    await asyncio.gather(*coroutines)

    return worker_result


async def commit_action(
    worker_config: WorkerConfig,
    contract: DeployedContract,
    recipient: str, amount: int, id: str,
    next_transaction_nonce,
    transaction_lock: Optional[asyncio.Lock] = None,
):
    is_confirmed_by_me, is_confirmed = await asyncio.gather(
        asyncio.to_thread(contract.confirmations(
            id, worker_config.my_address).call),
        asyncio.to_thread(contract.isConfirmed(id).call),
    )
    if is_confirmed_by_me or is_confirmed:
        logger.debug(f"action {id} was already confirmed; returning")
        return Result.OK

    logger.debug(f"commiting action {id}")
    coroutune = commit(
        contract,
        recipient, amount, id,
        next_transaction_nonce,
    )

    if transaction_lock is None:
        return await coroutune

    async with transaction_lock:
        return await coroutune


async def commit(
    contract: DeployedContract,
    recipient: str, amount: int, id: str,
    next_transaction_nonce,
):
    try:
        await asyncio.to_thread(
            contract.commit(
                recipient,
                amount,
                id,
            ).transact,
            nonce=next_transaction_nonce(),
            wait_for_receipt=False,
        )

        logger.debug(f"committed action {id}")
        return Result.OK
    except Exception as error:
        message = (
            error.args[0]["message"]
            if type(error.args[0]) == dict
            else ""
        )

        if ACTION_ALREADY_PERFORMED in message:
            logger.debug(f"action {id} was already confirmed; returning")
            return Result.OK

        if "nonce" in message:
            logger.warning(
                f"Detected an error due to parallel transactions: {error}. "
                "Switching to sequencial transactions."
            )
            return Result.DETECTED_NONCE_RACE

        logger.error(f"failed to commit action {id}: {error}")
        return Result.FAILED


def get_nonces_key(client: Client, token: str):
    field_position = f"{3:0>64}"
    address = f"{remove_0x_prefix(token).lower():0>64}"
    raw_key = address + field_position
    hashed_key = client.w3.toHex(client.w3.sha3(hexstr=raw_key))
    return remove_0x_prefix(hashed_key)
