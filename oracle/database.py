import sqlite3
from typing import Set, Dict, Tuple
from logging import getLogger

logger = getLogger("oracle.database")


class Database:
    def __init__(self, path: str, left_start_block: int, right_start_block: int):
        logger.debug(f"Connecting to {path}")
        self._connection = sqlite3.connect(path)
        cursor = self._connection.cursor()
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS actions (
                transaction_hash TEXT UNIQUE NOT NULL,
                recipient TEXT NOT NULL,
                amount TEXT NOT NULL,
                side INTEGER NOT NULL,
                processed INTEGER NOT NULL DEFAULT 0
            );
            """
        )
        cursor.execute(
            f"""
            CREATE TABLE IF NOT EXISTS state (
                left_last_block INTEGER NOT NULL DEFAULT {left_start_block},
                right_last_block INTEGER NOT NULL DEFAULT {right_start_block}
            );
            """
        )

        cursor.execute("SELECT count(1) as has_state FROM state LIMIT 1;")
        (has_state,) = cursor.fetchone()
        if not has_state:
            cursor.execute("INSERT INTO state DEFAULT VALUES;")

        cursor.close()

    def _side(self, side: str) -> int:
        return 0 if side == "left" else 1

    def get_last_block(self, side: str) -> int:
        cursor = self._connection.cursor()
        cursor.execute(f"SELECT {side}_last_block FROM state;")
        (last_block,) = cursor.fetchone()
        cursor.close()
        return last_block

    def set_last_block(self, side: str, last_block: int):
        cursor = self._connection.cursor()
        cursor.execute(
            f"UPDATE state SET {side}_last_block = ?;", (last_block,))
        cursor.close()
        self._connection.commit()

    def register_actions(self, side: str, actions: Dict[bytes, Tuple[str, int]]) -> int:
        side = self._side(side)

        cursor = self._connection.cursor()
        for transaction_hash, (recipient, amount) in actions.items():
            cursor.execute(
                """
                INSERT OR IGNORE
                    INTO actions (transaction_hash, recipient, amount, side)
                    VALUES (:transaction_hash, :recipient, :amount, :side);
                """,
                {
                    "transaction_hash": transaction_hash,
                    "recipient": recipient,
                    "amount": str(amount),
                    "side": side,
                },
            )

        cursor.close()
        self._connection.commit()

    def get_pending_actions(self, side: str) -> Dict[bytes, Tuple[str, int]]:
        side = self._side(side)
        cursor = self._connection.cursor()
        cursor.execute(
            """
            SELECT transaction_hash, recipient, amount
                FROM actions
                WHERE processed = 0 AND side = ?;
            """,
            (side,),
        )
        actions = {transaction_hash: (recipient, int(amount))
                   for (transaction_hash, recipient, amount) in cursor}
        cursor.close()
        return actions

    def drop_actions(self, actions: Set[str]):
        cursor = self._connection.cursor()
        for transaction_hash in actions:
            cursor.execute(
                "UPDATE actions SET processed = 1 WHERE transaction_hash = ?;",
                (transaction_hash,),
            )
        cursor.close()
        self._connection.commit()

    def has_pending_actions(self, side: str) -> bool:
        side = self._side(side)
        cursor = self._connection.cursor()
        cursor.execute(
            """
            SELECT count(1) as has_pending_actions
                FROM actions
                WHERE processed = 0 AND side = ?
                LIMIT 1;
            """,
            (side,),
        )
        (has_pending_actions,) = cursor.fetchone()
        cursor.close()
        return has_pending_actions > 0
