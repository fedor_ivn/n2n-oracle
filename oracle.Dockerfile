FROM python:latest

WORKDIR /oracle

ENV PYTHONUNBUFFERED=1
ENV DATABASE_PATH=/data/state.db
ENV BUILD_PATH=/oracle/contracts/build/contracts/

COPY /oracle .
COPY /contracts/build/ /oracle/contracts/build/

RUN pip install -r requirements.txt

#COPY ./wait-for-it.sh .
#RUN chmod +x ./wait-for-it.sh

CMD python main.py
