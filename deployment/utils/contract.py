from .client import Client


class ContractMethodCall:
    def __init__(self, client, method_call):
        self.client = client
        self.method_call = method_call

    def transact(self, **kwargs):
        return self._transact_method(self.method_call, **kwargs)

    def call(self, *args, **kwargs):
        return self.method_call.call(*args, **kwargs)

    def _transact_method(self, method_call, **kwargs):
        address = self.client.account.address

        wait_for_receipt = kwargs.pop("wait_for_receipt")
        tx = kwargs
        tx['from'] = address
        tx['gasPrice'] = self.client.gas_price
        if 'nonce' not in tx:
            tx['nonce'] = self.client.w3.eth.getTransactionCount(address)

        # print(self.client.gas_price)
        gas_estimated = method_call.estimateGas(tx)
        tx['gas'] = gas_estimated

        tx_wo_sign = method_call.buildTransaction(tx)
        tx_receipt = self.client.transact(
            tx_wo_sign, wait_for_receipt=wait_for_receipt)
        return tx_receipt


class ContractMethod:
    def __init__(self, client, method):
        self.client = client
        self.method = method

    def __call__(self, *args, **kwargs):
        method_call = self.method(*args, **kwargs)
        return ContractMethodCall(self.client, method_call)


class DeployedContract:
    """
    Класс-обертка над стандартными контрактами Web3

    Example:
        contract = DeployedContract(client, address=address, abi=abi)
        contract.balanceOf(arg1).call()
        contract.transfer(arg1, arg2).transact()
    """

    def __init__(self, client: Client, contract=None, address=None, abi=None):
        self.client = client

        if contract:
            self.contract = contract
        elif address and abi:
            self.contract = client.w3.eth.contract(address, abi=abi)
        else:
            raise ValueError

    def __getattr__(self, item):
        try:
            method = self.contract.functions[item]
        except ValueError:
            raise ValueError(f"There is no method {item} in contract's abi")

        return ContractMethod(self.client, method)

    def balance(self):
        return self.client.get_balance(self.contract.address)
