// SPDX-License-Identifier: None

pragma solidity >=0.7.5;

import "./BridgeValidators.sol";
import "./selfDestructContract.sol";

contract NativeTokenBridge {
    address public owner;
    address public validatorSet;
    uint256 liquidityLimit;
    uint256 initLiquidityLimit = 0;
    bool isRight;

    mapping(bytes32 => mapping(address => bool)) public confirmations;

    event bridgeActionInitiated(address recipient, uint256 amount);

    modifier onlyOwner() {
        require(owner == msg.sender, "Only owner can access this method.");
        _;
    }

    modifier isValuable() {
        require(msg.value > 0, "You should send more than 0 ether.");
        _;
    }

    modifier isInRange(bool addOrRemove) {
        if (addOrRemove) {
            require(liquidityLimit + msg.value <= initLiquidityLimit, "Too many tokens for the bridge.");
        } else {
            require(liquidityLimit >=  msg.value, "Too many tokens for the bridge...");
        }
        _;
    }

    constructor(address _validatorSet, bool _side) {
        owner = msg.sender;
        validatorSet = _validatorSet;
        isRight = _side;
    }

    receive() external payable isValuable isInRange(isRight) {
        emit bridgeActionInitiated(msg.sender, msg.value);
        if (isRight) {
            liquidityLimit += msg.value;
        } else {
            liquidityLimit -= msg.value;
        }
    }

    function changeValidatorSet(address newValidatorSet) public onlyOwner {
        require(validatorSet != newValidatorSet, "The same address.");
        validatorSet = newValidatorSet;
    }

    function addLiquidity() public payable onlyOwner isValuable {
        initLiquidityLimit +=  msg.value;
        liquidityLimit += msg.value;
    }

    function updateLiquidityLimit(uint256 newLiquidityLimit) public onlyOwner {
        liquidityLimit = newLiquidityLimit;
    }

    function getLiquidityLimit() public view returns (uint256) {
        return liquidityLimit;
    }

    function countConfirmations(bytes32 actionId)
        public
        view
        returns (uint256)
    {
        BridgeValidators validators = BridgeValidators(validatorSet);
        address[] memory data = validators.getValidators();

        uint256 count = 0;
        for (uint256 i = 0; i < data.length; i++) {
            if (confirmations[actionId][data[i]]) {
                count += 1;
            }
        }
        return count;
    }

    function confirmAction(bytes32 actionId) internal {
        require(
            !confirmations[actionId][msg.sender],
            "You've already confirmed this action."
        );
        confirmations[actionId][msg.sender] = true;
    }

    function isConfirmed(bytes32 actionId) public view returns(bool) {
        BridgeValidators validators = BridgeValidators(validatorSet);
        return countConfirmations(actionId) >= validators.getThreshold();
    }

    // function deleteAction(bytes32 actionId) internal {
    //     BridgeValidators validators = BridgeValidators(validatorSet);
    //     address[] memory data = validators.getValidators();

    //     for (uint i = 0; i < data.length; i++) {
    //         delete confirmations[actionId][data[i]];
    //     }
    // }

    function commit(
        address payable recipient,
        uint256 amount,
        bytes32 id
    ) public {
        BridgeValidators validators = BridgeValidators(validatorSet);
        require(
            validators.isValidator(msg.sender),
            "Only validator can access this method."
        );

        require(!isConfirmed(id), "The action was already perfomed.");

        confirmAction(id);

        if (countConfirmations(id) == validators.getThreshold()) {
            selfDestructContract DestructContract = new selfDestructContract(recipient);
            payable(DestructContract).transfer(amount);
            DestructContract.destroy();
            if (!isRight) {
                liquidityLimit += amount;
            } else {
                liquidityLimit -= amount;
            }
        }
    }
}
